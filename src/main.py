import torch

available_gpus = [torch.cuda.device(i) for i in range(torch.cuda.device_count())]

print(f"Number of available GPUs in this system available to pytorch: {available_gpus}")