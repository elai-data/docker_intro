#!/bin/sh

# The name of our docker image
image_name=docker_intro
version=1.0

# Build the image locally
docker build --network=host -t ${image_name}:${version} -f ./docker/Dockerfile . || exit 1

# Tag the image also with the :latest tag
docker tag ${image_name}:${version} ${image_name}:latest
