# Build a docker image

    scripts/build_image.sh

# Start the image - simple

    # Runs the default script, which is not defined, so does nothing
    docker run docker_intro:latest
    
    # Runs a bash shell in the container, allows to work in the container
    docker run -i -t docker_intro:latest
    
    # First add /host/data in Docker > Preferences > Resources > File Sharing
    docker run -it -v /host/data:/home/a_user/data docker_intro
    
# Start the image - docker-compose

    docker-compose -f docker/main/docker-compose.yml up
    
    # Detached mode:
    docker-compose -f docker/main/docker-compose.yml up -d
    
    docker-compose -f docker/main/docker-compose.yml down
    
# Look around

    # List all running and stopped containers
    docker ps -a
    
    # List all images
    docker image ls | grep docker_intro
    
    # Get the logs of a running or stopped container
    docker logs list_nr_of_gpus -f
    
    docker logs --tail 100 -f  list_nr_of_gpus
    
    docker volume ls

# Cleanup

    docker stop <container_name>
    
    # Cleanup all stopped containers
    docker container prune
     
    
    